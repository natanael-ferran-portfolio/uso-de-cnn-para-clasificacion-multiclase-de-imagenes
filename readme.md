# Uso de CNN para Clasificación Multiclase de Imágenes
## Data Scientist
- Ferrán, Natanael Emir

## Dataset
- [Fashion MNIST](https://www.kaggle.com/datasets/zalando-research/fashionmnist)
- **Licencia:** _The MIT License (MIT) Copyright © [2017] Zalando SE, https://tech.zalando.com_
- **Autor:** _AZalando Research_

## A resolver
- Clasificar las 10 distintas clases del dataset, variando los hiperparámetros de un modelo con capas de convolución.

## Técnicas utilizadas
- CNN
- Dropout
